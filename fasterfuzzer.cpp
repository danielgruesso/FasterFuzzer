/*
    Copyright 2010-2015 Adobe Systems Incorporated
    Copyright 2016-2018 Chris Cox
    Distributed under the MIT License (see accompanying file LICENSE_1_0_0.txt
    or a copy at http://stlab.adobe.com/licenses.html )
    

Goals:  A very fast fuzzer for binary files,  (tops out around 8 GB/s)
        portable to multiple platforms,        (currently tested on MacOS, Windows, Ubuntu)
        able to handle large files (over 4 GB),
        using a reasonable amount of RAM,    (16 to 128 Meg)
        with random number seeding for reproduceable results,
        that can target a specific range of bytes in the file to modify,
        and has a way to visualize the byte coverage of fuzzed files.
            (because managers love graphics)


*/

#include <cstdio>
#include <fstream>
#include <vector>
#include <algorithm>
#include <vector>
#include <iostream>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include <sys/stat.h>
#include <stdint.h>
using namespace std;

/******************************************************************************/

const char kVersionString[] = "0.8.1b";

/******************************************************************************/

typedef enum {
    kRandom,
    kMutate,
    kTrouble,
    kFlipBit,
    kInsert,
    kDelete,
    kBitSequence,
    kASCII
} fuzz_operation;

/******************************************************************************/

typedef fstream::off_type    pos_type;

typedef struct {
    pos_type        location;
    uint64_t        old_value;
} mutation;

static bool operator<(const mutation a, const mutation &b)
    {
    return a.location < b.location;
    }

typedef vector<mutation>    mutationVector;

/******************************************************************************/

#if WIN32
const char kPathSeparator = '\\';
#else
const char kPathSeparator = '/';
#endif

/******************************************************************************/

// TODO - ccox - can/should I make this into a single global parameter struct?

uint64_t gFuzzSeed = 1;                // 64 bit random seed

int gFuzzFileCount = 100;            // 1..INT_MAX

int gFuzzBytes = 1;                    // 1..8 = size of each location to modify

double gFuzzMutationRate = 0.05;    // 0..100.0   percentage of locations to modify
int gFuzzMutationCount = 0;            // number of locations to modify, calculated from rate or set directly

int64_t gFuzzMutationRange = 128;    // absolute value, must be positive

fuzz_operation gFuzzOperation = kRandom;        // type of fuzziing operation to perform

char *gOutputDir = NULL;            // name of output directory

int64_t gFuzzBeginPoint = 0;        // starting point of byte range to fuzz
int64_t gFuzzEndPoint = 0;            // end point of byte range to fuzz

bool gCoverageMode = false;        // just record file coverage in an image, a diagnostic for developers and paranoid QE managers

int gStreamBufferSize    = (int) (8*1024*1024L);        // 8 meg each for input and output

int64_t gInPlaceMemoryLimit = 64*1024L*1024L;    // 64 Meg in place edit buffer (works well with 2 or 4 Gig system)

/******************************************************************************/

static void print_usage(char *argv[])
{
    cout << "Usage: " << argv[0] << " <args> basefilename.ext\n";
    cout << "\t-seed S     Random number seed (default " << gFuzzSeed << ")\n";
    cout << "\t-files F    Number of new files to generate (default " << gFuzzFileCount << ")\n";
    cout << "\t-bytes B    Number of bytes to modify per location [1..8] (default " << gFuzzBytes << ")\n";
    cout << "\t-rate R     Percentage (float) of locations to modify (default " << gFuzzMutationRate << ")\n";
    cout << "\t-count C    Exact count of locations to modify (overrides rate, no default)\n";
    cout << "\t-range R    Modification range for mutation (default " << gFuzzMutationRange << ")\n";
    cout << "\t-outdir O   location of output directory (default current directory)\n";
    cout << "\t-style S    Style of modifications to perform (default random)\n";
    cout << "\t            one of: [ random | mutate | trouble | flipbit | ascii | bitsequential | insert | delete ]\n";
    cout << "\t-coverage   just record file coverage in an image (default off)\n";
    cout << "\t-start L    Starting location (in bytes) for fuzzing (default " << gFuzzBeginPoint << ")\n";
    cout << "\t-end L      Ending location (in bytes) for fuzzing (default EOF)\n";
    cout << "\t-buffer B   Number of bytes for read and write buffers (default " << gStreamBufferSize << ")\n";
    cout << "\t-limit M    Memory limit for in-place buffering (default " << gInPlaceMemoryLimit << ")\n";
    cout << "\t-version    Prints this message and exits immediately\n";
    cout << "Version " << kVersionString << ", Compiled " << __DATE__ << " " <<  __TIME__ << std::endl; // and flush on the last line
}

/******************************************************************************/
/******************************************************************************/

// SPRNG 64 bit LCRNG default:  not the best, but not bad
static uint64_t Hash64( uint64_t input )
{
    return (2862933555777941757ULL * (uint64_t)input + 3037000493ULL);
}

static uint64_t RandomGenerator64( uint64_t seed, int64_t input )
{
//    return Hash64( seed ^ Hash64( input ) );    // old, shows too many patterns
    return Hash64( Hash64(seed) ^ Hash64( input ) );    // new, no patterns found yet
}

/******************************************************************************/
/******************************************************************************/

static void create_seek_offsets( uint64_t seed, mutationVector &locations, pos_type limit )
{
    uint64_t seedmod = seed + 424242L;

    if (gFuzzOperation == kBitSequence)
        {
        int64_t loc = seed >> 3;
        if (loc >= limit)
            loc = limit-1;
        locations[0].location = pos_type( loc );
        return;
        }
        
    uint64_t fuzzed_range = gFuzzEndPoint - gFuzzBeginPoint;

    for (size_t i = 0; i < locations.size(); ++i)
        {
        
        // create random index into file
        uint64_t temp = gFuzzBeginPoint + ((RandomGenerator64( seedmod, i )) % fuzzed_range);
        
        if ( pos_type( temp ) >= limit )
            temp = limit - 1;
        
        // align down to fuzzbytes boundary
        if ( gFuzzBytes > 1 )
            {
            switch (gFuzzBytes)
                {
                default:
                    temp -= (temp % gFuzzBytes);
                    // aka temp /= gFuzzBytes;    temp *= gFuzzBytes;
                    break;
                case 2:
                    temp &= ~ uint64_t(1);
                    break;
                case 3:
                    temp -= (temp % 3);
                    break;
                case 4:
                    temp &= ~ uint64_t(3);
                    break;
                case 5:
                    temp -= (temp % 5);
                    break;
                case 6:
                    temp -= (temp % 6);
                    break;
                case 7:
                    temp -= (temp % 7);
                    break;
                case 8:
                    temp &= ~ uint64_t(7);
                    break;
                }

            }
        
        locations[i].location = pos_type( temp );
        }
}

/******************************************************************************/

static void generate_fuzzed_value( const uint64_t seed, const pos_type location, char buffer[8] )
    {
    const char kASCIIChars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890`~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/? \t\n\r";
    const int kASCIICharCount = (int)( sizeof(kASCIIChars) / sizeof(char) );

    uint64_t *u64_buf = (uint64_t *)buffer;
    
    uint64_t temp = RandomGenerator64(seed, location);
    
    // create new value
    switch( gFuzzOperation )
        {
        
        default:
        case kRandom:
        case kInsert:
        case kDelete:
            *u64_buf = temp;
            break;
        
        case kMutate:
            {
            int64_t mutate_value = 1 + (temp % gFuzzMutationRange);        // zero is not an interesting change
            if ((temp & 0x80000000L) != 0)        // use one bit to decide if we should negate or not
                mutate_value = -mutate_value;
            
            switch (gFuzzBytes)
                {
                default:
                case 1:
                    *((int8_t *)buffer) += (int8_t)mutate_value;
                    break;
                case 2:
                    *((int16_t *)buffer) += (int16_t)mutate_value;
                    break;
                case 4:
                    *((int32_t *)buffer) += (int32_t)mutate_value;
                    break;
                case 8:
                    *((int64_t *)buffer) += mutate_value;
                    break;
                }
            }
            break;
        
        case kTrouble:
            
            // use bits in the random value to determine our replacment value
            // creating common "trouble" values, symmetric so byte order isn't a problem
            if ((temp & 0x01) != 0)
                u64_buf[0] = uint64_t( -1 );
            else
                u64_buf[0] = 0;
            
            if ((temp & 0x02) != 0)
                buffer[0] ^= 0x80;
            
            if ((temp & 0x04) != 0)
                buffer[0] ^= 0x01;
            
            if (gFuzzBytes > 1)
                {
                if (((temp & 0x08) != 0))
                    buffer[gFuzzBytes-1] ^= 0x80;
                
                if (((temp & 0x10) != 0))
                    buffer[gFuzzBytes-1] ^= 0x01;
                }
            break;
        
        case kFlipBit:
            {
            int byte_number = (int) (temp % gFuzzBytes);
            int bit_number = (int) (temp >> 16) & 7;
            buffer[ byte_number ] ^= (1 << bit_number);
            }
            break;
        
        case kBitSequence:
            {
            int bit_number = (int) (seed & 7);
            buffer[ 0 ] ^= (1 << bit_number);
            }
            break;
        
        case kASCII:
            {
            *u64_buf = temp;
            
            // force the replacement values to be printable ASCII or space
            for ( int j = 0; j < gFuzzBytes; ++j )
                {
                buffer[j] &= 0x7f;    // clear high bits
                
                if ( !isgraph( (int) buffer[j] ) && !isspace( (int) buffer[j] ) )
                    {
                    // lookup a replacement
                    int index = (int) ( ((uint64_t)(seed + (int64_t)buffer[j])) % kASCIICharCount);
                    buffer[j] = kASCIIChars[ index ];
                    }
                }
            }
            break;
        }

    }

/******************************************************************************/

static void mutate_one_location_stream( const uint64_t seed, ofstream &output_file, ifstream &input_file, pos_type &location, const pos_type limit )
{
    char buffer[ 8 ];
    
    // be safe
    if ( (gFuzzOperation != kInsert)
        && (limit - location) < gFuzzBytes)
        return;
    
    // eat this many bytes of input
    input_file.read ( buffer, gFuzzBytes );
    
    if (gFuzzOperation == kInsert)
        output_file.write( buffer, gFuzzBytes );
    
    // create new value
    generate_fuzzed_value( seed, location, buffer );
    
    // write out our new values
    if (gFuzzOperation != kDelete)
        output_file.write( buffer, gFuzzBytes );
    
    // increment our input location
    location += gFuzzBytes;

}

/******************************************************************************/

static void mutate_one_location_inplace( const uint64_t seed, char *input_buffer, const pos_type location, const pos_type limit, uint64_t &old_value )
{
    char buffer[ 8 ];
    uint64_t *u64_buf = (uint64_t *)buffer;
    
    // be safe
    if ( (gFuzzOperation != kInsert)
        && (limit - location) < gFuzzBytes)
        return;
    
    if (gCoverageMode)
        {
        // increment each affected byte by one, until we reach 255
        unsigned char *buf = (unsigned char *)( input_buffer+location );
        for (int j = 0; j < gFuzzBytes; ++j)
            {
            unsigned char val = buf[j];
            if (val < 255)    val++;
            buf[j] = val;
            }
        return;
        }
    
    // copy this many bytes of input
    memcpy( buffer, input_buffer+location, gFuzzBytes );
    old_value = *u64_buf;
    
    // create new value
    generate_fuzzed_value( seed, location, buffer );
    
    // write out our new values
    memcpy( input_buffer+location, buffer, gFuzzBytes );
}

/******************************************************************************/

static void restore_one_location_inplace( char *input_buffer, pos_type &location, const pos_type limit, uint64_t &old_value )
{
    // be safe
    if ( (gFuzzOperation != kInsert)
        && (limit - location) < gFuzzBytes)
        return;
    
    // copy the original value back where it started
    memcpy( input_buffer+location, &old_value, gFuzzBytes );
}

/******************************************************************************/

static void copy_stream_bytes( ofstream &output_file, ifstream &input_file, pos_type count )
{
    const std::streamsize BLOCK_SIZE = std::max( gStreamBufferSize / 8, 128);
    static std::vector<char> storage(BLOCK_SIZE);   // static to reduce stack thrashing and allocations
    char *local_block = &storage[0];
    
    while ( count >= BLOCK_SIZE )
        {
        input_file.read ( local_block, BLOCK_SIZE );
        output_file.write ( local_block, BLOCK_SIZE );
        count -= BLOCK_SIZE;
        }
    
    if (count > 0)
        {
        input_file.read ( local_block, (std::streamsize) count );
        output_file.write ( local_block, (std::streamsize) count );
        }
}

/******************************************************************************/

static
std::string createFuzzedName( const char *template_filename, uint64_t seed )
{
    // construct the output filename, including any terminating extension
    std::string base_name = template_filename;
    std::string extension;
    size_t extension_start = base_name.find('.');
    
    if (extension_start != std::string::npos)
        {
        extension = base_name.substr(extension_start);
        base_name = base_name.substr(0,extension_start);
        }
    
    //snprintf( name_buffer, new_length, "%s_%llu%s", base_name, seed, extension );
    
    std::string result = base_name + "_" + std::to_string(seed) + extension;
    
    return result;
}

/******************************************************************************/

static void fuzz_one_file_stream(const char *template_filename, uint64_t seed,
                                ifstream &input_file, pos_type input_size, char *output_buffer )
{
    // seek to beginning of input
    input_file.seekg( 0, ios::beg );
    
    std::string name_buffer = createFuzzedName( template_filename, seed );
    
    // create the output file
    ofstream output_file;
    if (output_buffer != NULL)
        output_file.rdbuf()->pubsetbuf( output_buffer, gStreamBufferSize );
    
    output_file.open( name_buffer, ios::out | ios::binary | ios::trunc );
    
    if (!output_file.good() || output_file.fail())
        {
        cout << "ERROR: could not create output file " << name_buffer << "\n";
        exit(-1);
        }

    // create our list of mutation points
    mutationVector locations;
    locations.resize( gFuzzMutationCount );
    create_seek_offsets( seed, locations, input_size );
    
    // sort the list of mutation points in increasing order
    sort( locations.begin(), locations.end() );
    
    // copy the input to output, until we hit a mutation point, do work, continue
    pos_type location = 0;

    for (size_t i = 0; i < locations.size(); ++i )
        {
        pos_type next_mutation = locations[i].location;
        
        if (next_mutation > location)
            {
            // copy up to the mutation point
            pos_type copy_length = next_mutation - location;
            
            copy_stream_bytes( output_file, input_file, copy_length );
            location = next_mutation;
            
            mutate_one_location_stream( seed, output_file, input_file, location, input_size );
            }
        }
    
    // copy any remaining bytes in the file
    pos_type leftover = input_size - location;
    if (leftover > 0)
        copy_stream_bytes( output_file, input_file, leftover );

    if (output_file.fail())
        {
        cout << "ERROR: output file problem on seed " << seed << "\n";
        }

    // we're done with this file, close the output
    output_file.close();

}

/******************************************************************************/

static void fuzz_one_file_inplace(const char *template_filename, uint64_t seed,
                                    char *input_buffer, pos_type input_size, char *output_buffer)
{
    // do some error checking first
    if ( gFuzzOperation == kInsert
        || gFuzzOperation ==  kDelete)
        {
        cout << "ERROR: insert and delete operations cannot be done inplace\n";
        exit(-1);
        }
    
    ofstream output_file;

    if (!gCoverageMode)
        {
        std::string name_buffer = createFuzzedName( template_filename, seed );
        
        if (output_buffer != NULL)
            output_file.rdbuf()->pubsetbuf( output_buffer, input_size );
        
        // create the output file
        output_file.open( name_buffer, ios::out | ios::binary | ios::trunc );
        
        if (!output_file.good() || output_file.fail())
            {
            cout << "ERROR: could not create output file " << name_buffer << "\n";
            exit(-1);
            }
        
        }    // !gCoverageMode
    
    // create our list of mutation points
    mutationVector locations;
    locations.resize( gFuzzMutationCount );
    create_seek_offsets( seed, locations, input_size );
    
    // sort the list of mutation points in increasing order
    sort( locations.begin(), locations.end() );
    
    // mutate the locations chosen, store the original values
    for (size_t i = 0; i < locations.size(); ++i )
        {
        mutate_one_location_inplace( seed, input_buffer, locations[i].location, input_size, locations[i].old_value );
        }
    
    if (!gCoverageMode)
        {
        // write the buffer to disk
        output_file.write ( input_buffer, (std::streamsize) input_size );
        
        if (output_file.fail())
            {
            cout << "ERROR: output file problem on seed " << seed << "\n";
            // try to continue
            }
        
        // we're done with this file, close the output
        output_file.close();
        
        // restore the original buffer values
        for (size_t i = 0; i < locations.size(); ++i )
            {
            restore_one_location_inplace( input_buffer, locations[i].location, input_size, locations[i].old_value );
            }
        }

}

/******************************************************************************/

static void write_PGM_coverage(const char *template_filename, char *input_buffer, pos_type input_size)
{
    std::string name_buffer(template_filename);
    name_buffer += "_coverage.pgm";
    
    // create the output file
    ofstream output_file;
    output_file.open( name_buffer, ios::out | ios::binary | ios::trunc );
    
    if (!output_file.good() || output_file.fail())
        {
        cout << "ERROR: could not create output file " << name_buffer << "\n";
        exit(-1);
        }
    
    // guess the best rectangle size to write image (so it isn't one long row)
    pos_type width = 0;
    pos_type height = 0;
    pos_type extra_bytes = 0;
    
    width = (pos_type) sqrt( input_size );
    height = (input_size + (width-1)) / width;
    extra_bytes = (width * height) - input_size;
    
    
    // write PGM header
    output_file << "P5\n" << width << " " << height << "\n255\n";
    
    // write the coverage buffer to disk
    output_file.write ( input_buffer, (std::streamsize) input_size );
    
    if (output_file.fail())
        {
        cout << "ERROR: output file writing problem\n";
        }
    
    // write extra bytes to finish out rectangle
    if (extra_bytes > 0)
        {
        char *temp = new char [ extra_bytes ];
        memset( temp, 255, extra_bytes );
        output_file.write ( temp, (std::streamsize) extra_bytes );
        delete[] temp;
        
        if (output_file.fail())
            {
            cout << "ERROR: output file writing problem\n";
            }
        }
    
    // we're done with this file, close the output
    output_file.close();

}


/******************************************************************************/

static void parse_arguments( int argc, char *argv[] )
{
    if (argc < 2)
        {
        print_usage( argv );
        exit (-1);
        }

    for ( int c = 1; c < argc; ++c )
        {
        
        if ( strcmp( argv[c], "-seed" ) == 0
            && c < (argc-1) )
            {
            gFuzzSeed = (uint64_t)atoll( argv[c+1] );
            ++c;
            }
        else if ( strcmp( argv[c], "-files" ) == 0
            && c < (argc-1) )
            {
            gFuzzFileCount = atoi( argv[c+1] );
            if (gFuzzFileCount < 1)
                {
                cout << "INFO: Invalid file count (" << gFuzzFileCount << "), pinning to 1\n";
                gFuzzFileCount = 1;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-bytes" ) == 0
            && c < (argc-1) )
            {
            gFuzzBytes = atoi( argv[c+1] );
            if (gFuzzBytes < 1)
                {
                cout << "INFO: Invalid byte count (" << gFuzzBytes << "), pinning to 1\n";
                gFuzzBytes = 1;
                }
            if (gFuzzBytes > 8)
                {
                cout << "INFO: Invalid byte count (" << gFuzzBytes << "), pinning to 8\n";
                gFuzzBytes = 8;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-rate" ) == 0
            && c < (argc-1) )
            {
            gFuzzMutationRate = atof( argv[c+1] );
            if (gFuzzMutationRate < 0.0)
                {
                cout << "INFO: Invalid mutation rate (" << gFuzzMutationRate << "), pinning to 0.0\n";
                gFuzzMutationRate = 0.0;
                }
            if (gFuzzMutationRate > 100.0)
                {
                cout << "INFO: Invalid mutation rate (" << gFuzzMutationRate << "), pinning to 100.0\n";
                gFuzzMutationRate = 100.0;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-buffer" ) == 0
            && c < (argc-1) )
            {
            gStreamBufferSize = atoi( argv[c+1] );
            if (gStreamBufferSize < 100)
                {
                cout << "INFO: Invalid buffer size (" << gStreamBufferSize << "), pinning to 100\n";
                gStreamBufferSize = 100;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-count" ) == 0
            && c < (argc-1) )
            {
            gFuzzMutationCount = atoi( argv[c+1] );
            if (gFuzzMutationCount < 1)
                {
                cout << "INFO: Invalid mutation count (" << gFuzzMutationCount << "), pinning to 1\n";
                gFuzzMutationCount = 1;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-coverage" ) == 0 )
            {
            gCoverageMode = true;
            }
        else if ( strcmp( argv[c], "-range" ) == 0
            && c < (argc-1) )
            {
            gFuzzMutationRange = atoll( argv[c+1] );
            if (gFuzzMutationRange < 1)
                {
                cout << "INFO: Invalid mutation range (" << gFuzzMutationRange << "), pinning to 1\n";
                gFuzzMutationRange = 1;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-start" ) == 0
            && c < (argc-1) )
            {
            gFuzzBeginPoint = atoll( argv[c+1] );
            if (gFuzzBeginPoint < 0)
                {
                cout << "INFO: Invalid start location (" << gFuzzBeginPoint << "), pinning to 0\n";
                gFuzzBeginPoint = 0;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-end" ) == 0
            && c < (argc-1) )
            {
            gFuzzEndPoint = atoll( argv[c+1] );
            if (gFuzzEndPoint < 0)
                {
                cout << "INFO: Invalid ending location (" << gFuzzEndPoint << "), pinning to EOF\n";
                gFuzzEndPoint = 0;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-limit" ) == 0
            && c < (argc-1) )
            {
            int64_t temp = atoll( argv[c+1] );
            if (temp < 1) {
                cout << "INFO: Invalid memory limit (" << temp << "), leaving at default value (" << gInPlaceMemoryLimit << ")\n";
                }
            else
                gInPlaceMemoryLimit = temp;
            ++c;
            }
        else if ( strcmp( argv[c], "-outdir" ) == 0
            && c < (argc-1) )
            {
            gOutputDir = argv[c+1];
            
            struct stat st;
            if( stat(gOutputDir,&st) != 0 )
                {
                cout << "ERROR: Output directory " << gOutputDir << " does not exist\n";
                exit(-1);
                }
            
            if( (st.st_mode & S_IFDIR) == 0 )
                {
                cout << "ERROR: " << gOutputDir << " is not a directory\n";
                exit(-1);
                }
            
            ++c;
            }
        else if ( strcmp( argv[c], "-style" ) == 0
            && c < (argc-1) )
            {
            char first_char = argv[c+1][0];
            
            switch( first_char )
                {
                default:
                cout << "INFO: unknown fuzzing style (" << argv[c+1] << "), using random\n";
                case 'r':
                case 'R':
                    gFuzzOperation = kRandom;
                    break;
                
                case 'm':
                case 'M':
                    gFuzzOperation = kMutate;
                    break;
                    
                case 't':
                case 'T':
                    gFuzzOperation = kTrouble;
                    break;
                    
                case 'f':
                case 'F':
                    gFuzzOperation = kFlipBit;
                    break;
                    
                case 'i':
                case 'I':
                    gFuzzOperation = kInsert;
                    break;
                    
                case 'd':
                case 'D':
                    gFuzzOperation = kDelete;
                    break;
                
                case 'b':
                case 'B':
                    gFuzzOperation = kBitSequence;
                    break;
                
                case 'a':
                case 'A':
                    gFuzzOperation = kASCII;
                    break;
                }
            
            ++c;
            }
        else if ( strcmp( argv[c], "-V" ) == 0
                || strcmp( argv[c], "-v" ) == 0
                || strcmp( argv[c], "-help" ) == 0
                || strcmp( argv[c], "--help" ) == 0
                || strcmp( argv[c], "-version" ) == 0
                || strcmp( argv[c], "-Version" ) == 0
                )
            {
            print_usage( argv );
            exit (0);
            }
        else if (argv[c][0] == '-')
            {
            // unrecognized switch
            print_usage( argv );
            exit (1);
            }
        
        }
    
    if (gFuzzOperation == kMutate)
        {
        if (gFuzzBytes != 1 && gFuzzBytes != 2 && gFuzzBytes != 4 && gFuzzBytes != 8)
            {
            cout << "ERROR: Can't do math on " << gFuzzBytes << " byte quantities yet\n";
            exit(-1);
            }
        }
    
    if (gFuzzOperation == kBitSequence)
        {
        gFuzzBytes = 1;
        gFuzzMutationCount = 1;
        }
}

/******************************************************************************/

int main (int argc, char *argv[])
{
    const bool using32bitSizes = (sizeof(ptrdiff_t) < 8);
    bool usingInPlaceBuffer = false;
    
    parse_arguments( argc, argv );

    const char *input_filename = argv[argc-1];
    
    
    // get size of input first
    ifstream temp_file;
    temp_file.open( input_filename, ios::in | ios::binary );
    temp_file.seekg( 0, ios::end );
    pos_type input_size = temp_file.tellg();
    
    if (input_size <= 0 || temp_file.fail())
        {
        cout << "ERROR: Could not open input file " << input_filename << "\n";
        return -1;
        }
    
    temp_file.close();


    // decide if we can do this in-place, or have to stream it
    if (input_size <= gInPlaceMemoryLimit
        && gFuzzOperation != kInsert
        && gFuzzOperation != kDelete)
        usingInPlaceBuffer = true;
    
    if (gCoverageMode)    // this requires an inplace buffer
        {
        usingInPlaceBuffer = true;
        
        if ( gFuzzOperation == kInsert
            || gFuzzOperation == kDelete)
            {
            cout << "ERROR: Coverage mode won't work with insert or delete operations\n";
            return -1;
            }
        }
    
    // 32 bit overflow would be a bad idea
    if (using32bitSizes && input_size >= (2*1024LL*1024LL*1024LL))
        {
        if (gCoverageMode)    // this requires an inplace buffer
            {
            cout << "INFO: Could not generate coverage for " << input_filename << " with 32 bit addressing\n";
            return -1;
            }
        
        if (usingInPlaceBuffer)
            {
            cout << "INFO: Could not use inplace buffer for " << input_filename << " with 32 bit addressing\n";
            }
        
        usingInPlaceBuffer = false;
        }
    
    char *input_buffer = NULL;
    char *output_buffer = NULL;
    ifstream input_file;

    if (usingInPlaceBuffer)
        {
        // we can do this in-place
        input_buffer = new char[ input_size ];
        output_buffer = new char[ input_size ];
        }
    else
        {
        // we'll have to stream it
        input_buffer = new char[ gStreamBufferSize ];
        output_buffer = new char[ gStreamBufferSize ];
        input_file.rdbuf()->pubsetbuf( input_buffer, gStreamBufferSize );
        }
    
    // open the intput file
    input_file.open(input_filename, ios::in | ios::binary );
    
    if (gFuzzEndPoint == 0 || gFuzzEndPoint > input_size)
        {
        gFuzzEndPoint = input_size;
        }
    
    int64_t fuzzed_range = gFuzzEndPoint - gFuzzBeginPoint;
    
    // setup and convert arguments to internal values
    if (gFuzzMutationCount == 0)
        gFuzzMutationCount = (int)( gFuzzMutationRate * (double)fuzzed_range / (100 * gFuzzBytes) );
    
    if (gFuzzMutationCount < 1)
        gFuzzMutationCount = 1;
    
    pos_type mutation_upper_limit = fuzzed_range / gFuzzBytes;
    
    if ( gFuzzMutationCount > mutation_upper_limit )
        gFuzzMutationCount = (int)mutation_upper_limit;
    
    // construct the name of the input file, in the output directory, as a template
    const char *basename = strrchr( input_filename, kPathSeparator );
    if (basename != NULL)
        basename++;
    else
        basename = input_filename;
    
    const int basename_length = (int) strlen( basename );
    
    const int path_length = (gOutputDir == NULL) ? 0 : (int)strlen( gOutputDir );
    const bool hasTerm = (path_length > 0) && (gOutputDir[ path_length-1 ] == kPathSeparator);
    
    const int combined_length = basename_length + path_length + 4;
    char *template_filename = new char [ combined_length ];
    template_filename[0] = 0;
    if (gOutputDir != NULL)
        {
        strcpy( template_filename, gOutputDir );
        if (!hasTerm)
            {
            template_filename[ path_length ] = kPathSeparator;
            template_filename[ path_length+1 ] = 0;
            }
        }
    strcat( template_filename, basename );
    
    if (usingInPlaceBuffer)
        {
        // read input into a buffer
        input_file.read ( input_buffer, (std::streamsize) input_size );
        
        // we're done reading the input
        input_file.close();
        }
    
    if (gCoverageMode)
        {
        // zero all the bytes of input
        memset( input_buffer, 0, (size_t) input_size );
        }
    
    // loop and generate each output file
    for ( int i = 0; i < gFuzzFileCount; ++i, ++gFuzzSeed )
        {
        if (usingInPlaceBuffer)
            fuzz_one_file_inplace( template_filename, gFuzzSeed, input_buffer, input_size, output_buffer );
        else
            {
            fuzz_one_file_stream( template_filename, gFuzzSeed, input_file, input_size, output_buffer );
            if (input_file.fail())
                {
                cout << "ERROR: input file problem on seed " << gFuzzSeed << "\n";
                delete[] template_filename;
                return -1;
                }
            }
        }

    // we're done, close the input
    if (!usingInPlaceBuffer)
        input_file.close();
    
    if (gCoverageMode)
        {
        // write out PGM file with coverage values in output directory
        write_PGM_coverage( template_filename, input_buffer, input_size );
        }
    
    // cleanup our buffers
    delete[] input_buffer;
    delete[] output_buffer;
    delete[] template_filename;
    
    return 0;
}

/******************************************************************************/
/******************************************************************************/
