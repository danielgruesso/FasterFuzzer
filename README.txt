/*  Copyright 2010-2015 Adobe Systems Incorporated
    Copyright 2016-2018 Chris Cox
    Distributed under the MIT License (see accompanying file LICENSE_1_0_0.txt
    or a copy at http://stlab.adobe.com/licenses.html )
*/

/******************************************************************************/

Goals:
    A very fast fuzzer for binary files,  (tops out around 8 GB/s)
    portable to multiple platforms,  (currently tested on MacOS, Windows, Ubuntu)
    able to handle large files (over 4 GB),
    using a reasonable amount of RAM,  (16 to 128 Meg)
    with random number seeding for reproduceable results,
    that can target a specific range of bytes in the file to modify,
    and has a way to visualize the byte coverage of fuzzed files
        (because managers love graphics)


/******************************************************************************/

Building:

Unix users should be able to use "make all".  If you wish to use a different
compiler, you can set that from the make command line, or edit the makefile.

Windows users will need to make sure that the VC environment variables
are set for their shell (command prompt), then use "nmake -f makefile.nt all"
from within that shell.

XCode and MSVisualStudio project files are included, but may not always be
up to the latest IDE versions.
