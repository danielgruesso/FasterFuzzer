/*
    Copyright 2010 Adobe Systems Incorporated
    Copyright 2018 Chris Cox
    Distributed under the MIT License (see accompanying file LICENSE_1_0_0.txt
    or a copy at http://stlab.adobe.com/licenses.html )


Goal:    A quick program to find a string and overwrite with a different string
            ie: replace NULL with 0x11223344
            ie: replace '"' with 'string_haz_no_end'
            ie: replace "</dl>" with "list_unterminated"

*/

#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <vector>
#include <string.h>
#include <stdint.h>
using namespace std;

#if WIN32
//#define atoll    _atoi64
#endif

/******************************************************************************/

const char kVersionString[] = "0.8.1b";

/******************************************************************************/

typedef fstream::off_type    pos_type;

/******************************************************************************/

uint8_t *gInputPattern = NULL;        // pattern we're searching for
int gInputLength = 0;
uint8_t kDefaultInputPattern[] = { 0, 0 };
int kDefaultInputLength = 1;

uint8_t *gOutputPattern = NULL;        // string we'll use to replace the found pattern
int gOutputLength = 0;
uint8_t kDefaultOutputPattern[] = { 0x55, 0 };
int kDefaultOutputLength = 1;

const int kPatternLengthLimit = 128;

const char *gOutputFilename = NULL;            // name of output directory
const char defaultOutputName[] = "smashed.out";

int64_t gFuzzBeginPoint = 0;        // starting point of byte range to fuzz
int64_t gFuzzEndPoint = 0;            // end point of byte range to fuzz

int gStreamBufferSize    = 8*1024*1024L;        // 8 meg each for input and output

/******************************************************************************/

static void print_usage(char *argv[])
{
    cout << "Usage: " << argv[0] << " <args> inputfilename.ext\n";
    cout << "\t-o Outfile      output file name (default " << defaultOutputName << ")\n";
    cout << "\t-target T       string to search for and replace (default NULL)\n";
    cout << "\t-replacement R  string to be used as replacement (default 0x55)\n";
    cout << "\t-start L        Starting location (in bytes) for search (default " << gFuzzBeginPoint << ")\n";
    cout << "\t-end L          Ending location (in bytes) for search (default EOF)\n";
    cout << "\t-buffer B       Number of bytes for read and write buffers (default " << gStreamBufferSize << ")\n";
    cout << "\t-version        Prints this message and exits immediately\n";
    cout << "Version " << kVersionString << ", Compiled " << __DATE__ << " " <<  __TIME__ << std::endl; // and flush on the last line
}

/******************************************************************************/
/******************************************************************************/

static void copy_stream_bytes( ofstream &output_file, ifstream &input_file, pos_type count )
{
    const std::streamsize BLOCK_SIZE = std::max( gStreamBufferSize / 8, 128);
    static std::vector<char> storage(BLOCK_SIZE);   // static to reduce stack thrashing and allocations
    char *local_block = &storage[0];
    
    while ( count >= BLOCK_SIZE )
        {
        input_file.read ( local_block, BLOCK_SIZE );
        output_file.write ( local_block, BLOCK_SIZE );
        count -= BLOCK_SIZE;
        }
    
    if (count > 0)
        {
        input_file.read ( local_block, (std::streamsize) count );
        output_file.write ( local_block, (std::streamsize) count );
        }
}

/******************************************************************************/

static void fuzz_one_file(const char *out_filename, ifstream &input_file, pos_type input_size, char *output_buffer)
{
    // seek to beginning of input
    input_file.seekg( 0, ios::beg );
    
    // create the output file
    ofstream output_file;
    if (output_buffer != NULL)
        output_file.rdbuf()->pubsetbuf( output_buffer, gStreamBufferSize );
    output_file.open( out_filename, ios::out | ios::binary | ios::trunc );
    
    if (!output_file.good() || output_file.fail())
        {
        cout << "ERROR: could not create output file " << out_filename << "\n";
        exit(-1);
        }


// process the file
    const std::streamsize BLOCK_SIZE = std::max( gStreamBufferSize / 8, 128);
    static std::vector<char> storage(BLOCK_SIZE);   // static to reduce stack thrashing and allocations
    char *local_block = &storage[0];
    std::streamsize file_pos = 0;
    
    if (gInputLength > BLOCK_SIZE)
        {
        cout << "ERROR: input string length of " << gInputLength << " is greater than buffer size " << BLOCK_SIZE << "\n";
        exit(-1);
        }

    if (file_pos < gFuzzBeginPoint)
        {
        copy_stream_bytes( output_file, input_file, gFuzzBeginPoint );
        file_pos = (std::streamsize) gFuzzBeginPoint;
        }
    
    std::streamsize replace_pos = 0;
    
    while ( file_pos < gFuzzEndPoint )
        {
        std::streamsize buffer_count = BLOCK_SIZE;
        
        if (file_pos + buffer_count > gFuzzEndPoint)
            buffer_count = (std::streamsize)(gFuzzEndPoint - file_pos);
        
        input_file.read ( local_block, buffer_count );
        
        std::streamsize input_pos = 0;
        
        
        // we may have leftover bytes to copy from previous loop
        if (replace_pos > 0)
            {
            // finish replace
            std::streamsize left_in_buffer = buffer_count;
            bool replace_overrun = buffer_count < (gOutputLength - replace_pos);
            
            if (replace_overrun)
                {
                memcpy( local_block+0, gOutputPattern+replace_pos, (size_t)left_in_buffer );
                replace_pos += left_in_buffer;
                input_pos = left_in_buffer;
                }
            else
                {
                memcpy( local_block+0, gOutputPattern+replace_pos, (size_t)(gOutputLength - replace_pos) );
                input_pos += gOutputLength - replace_pos;
                replace_pos = 0;
                }

            }

        
        // normal search
        while (input_pos < buffer_count )
            {
                    
            if ( local_block[ input_pos ] == gInputPattern[ 0 ] )
                {
                // we have a potential match
                
                // make sure we aren't trying to read over the end of the file
                if ( file_pos+input_pos+gInputLength > gFuzzEndPoint)
                    break;

                // ccox - if the search runs over the end of the buffer, we have to seek
                // and make sure we have the full search string in the next buffer
                // because backing up to overwrite would be ugly and slow

                // is this search string going to run over the end of the buffer?
                std::streamsize left_in_buffer = buffer_count - input_pos;
                bool search_overrun = left_in_buffer < gInputLength;
                
                if (search_overrun)
                    {
                    if (
                        memcmp( local_block+input_pos+1, (const char *)gInputPattern+1, (size_t)(left_in_buffer-1)) == 0)
                        {
                        // it matches what we've got, reduce amount to write in output
                        buffer_count = input_pos;
                        // and seek so we can restart on the next full buffer
                        input_file.seekg( file_pos+input_pos, ios::beg );
                        break;    // continue search in next buffer full
                        }
                    // else it doesn't match and we should continue
                    }
                else
                    {
                    bool matched = true;

// simple version for now
// see if we can test the end char as well
// if string is short, use unrolled loop instead of memcmp
                    if (matched
                        && gInputLength > 1
                        && memcmp( local_block+input_pos+1, (const char *)gInputPattern+1, (size_t)(gInputLength-1)) != 0)
                        matched = false;

                    
                    if (matched)
                        {
                        // start replacing
                        bool replace_overrun = left_in_buffer < gOutputLength;
                        
                        if (replace_overrun)
                            {
                            memcpy( local_block+input_pos, gOutputPattern, (size_t)left_in_buffer );
                            replace_pos = left_in_buffer;
                            break;    // continue replacing in next buffer full
                            }
                        else
                            {
                            memcpy( local_block+input_pos, gOutputPattern, (size_t)gOutputLength );
                            input_pos += gOutputLength-1;
                            }
                        }
                    }
                
                }
            
            ++input_pos;
            }


        output_file.write ( local_block, buffer_count );
        
        file_pos += buffer_count;
        }
    
    if (gFuzzEndPoint < input_size)
        copy_stream_bytes( output_file, input_file, (input_size - gFuzzEndPoint) );
    
    if (output_file.fail())
        cout << "ERROR: output file problem\n";

    // we're done with this file, close the output
    output_file.close();

}

/******************************************************************************/

static int hex_decode( char input, uint8_t &output )
{
    int error = 0;
    
    // This does assume ASCII like encodings.
    // If we ever port to EBCDIC, let me know.
    switch (input)
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            output = input - '0';
            break;
        
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
            output = 10 + input - 'a';
            break;
        
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
            output = 10 + input - 'A';
            break;
    
        default:
            error = 1;
            output = 0;
        }
    
    if (output > 15)
        {
        error = 1;        // ok, we have a character encoding failure
        cout << "hex decoding failure\n";
        output = 0;
        }
    
    return error;
}

/******************************************************************************/

static uint8_t * parse_pattern_argument( char *argv, int pattern_limit, int &result_length )
{
    uint8_t *pattern = NULL;
    int arg_length = (int) strlen( argv );

    // does the pattern start with 0x and contain valid hex values? - if so, treat it as a long hex string
    if ( arg_length > 2 && argv[0] == '0' && (argv[1] == 'x' || argv[1] == 'X') )
        {
        result_length = (arg_length - 2) / 2;
        if (pattern_limit && result_length > pattern_limit)
            result_length = pattern_limit;
        pattern = new uint8_t[ result_length ];
        
        int i, pos;
        for (i = 0, pos = 0; i < result_length; ++i, pos+=2)
            {
            uint8_t first_nibble = 0;
            uint8_t second_nibble = 0;
            
            int err = hex_decode( argv[ 2+pos + 0 ], first_nibble );
            if (err != 0)
                {
                delete[] pattern;
                pattern = NULL;
                break;
                }
            
            err = hex_decode( argv[ 2+pos + 1 ], second_nibble );
            if (err != 0)
                {
                delete[] pattern;
                pattern = NULL;
                break;
                }
            
            uint8_t byte_value = (first_nibble << 4) | second_nibble;
            
            pattern[i] = byte_value;
            }
        }
    
    if (pattern == NULL)
        {
        // if hex parsing failed, treat the pattern as a literal string
        pattern = (uint8_t *) argv;
        result_length = arg_length;
        if (pattern_limit && result_length > pattern_limit)
            result_length = pattern_limit;
        }
    
    return pattern;
}

/******************************************************************************/

static void parse_arguments( int argc, char *argv[] )
{
    if (argc < 2)
        {
        print_usage( argv );
        exit (-1);
        }

    for ( int c = 1; c < argc; ++c )
        {
        
        if ( (strcmp( argv[c], "-target" ) == 0 || strcmp( argv[c], "-t" ) == 0 )
            && c < (argc-1) )
            {
            gInputPattern = parse_pattern_argument( argv[c+1], kPatternLengthLimit, gInputLength );
            ++c;
            }
        else if ( (strcmp( argv[c], "-replacement" ) == 0 || strcmp( argv[c], "-r" ) == 0 )
            && c < (argc-1) )
            {
            gOutputPattern = parse_pattern_argument( argv[c+1], kPatternLengthLimit, gOutputLength ); 
            ++c;
            }
        else if ( (strcmp( argv[c], "-start" ) == 0 || strcmp( argv[c], "-s" ) == 0 )
            && c < (argc-1) )
            {
            gFuzzBeginPoint = atoll( argv[c+1] );
            if (gFuzzBeginPoint < 0)
                {
                cout << "INFO: Invalid start location (" << gFuzzBeginPoint << "), pinning to 0\n";
                gFuzzBeginPoint = 0;
                }
            ++c;
            }
        else if ( (strcmp( argv[c], "-end" ) == 0|| strcmp( argv[c], "-e" ) == 0 )
            && c < (argc-1) )
            {
            gFuzzEndPoint = atoll( argv[c+1] );
            if (gFuzzEndPoint < 0)
                {
                cout << "INFO: Invalid ending location (" << gFuzzEndPoint << "), pinning to EOF\n";
                gFuzzEndPoint = 0;
                }
            ++c;
            }
        else if ( (strcmp( argv[c], "-output" ) == 0 || strcmp( argv[c], "-o" ) == 0 )
            && c < (argc-1) )
            {
            gOutputFilename = argv[c+1];
            ++c;
            }
        else if ( strcmp( argv[c], "-buffer" ) == 0
            && c < (argc-1) )
            {
            gStreamBufferSize = atoi( argv[c+1] );
            if (gStreamBufferSize < 100)
                {
                cout << "INFO: Invalid buffer size (" << gStreamBufferSize << "), pinning to 100\n";
                gStreamBufferSize = 100;
                }
            ++c;
            }
        else if ( strcmp( argv[c], "-V" ) == 0
                || strcmp( argv[c], "-v" ) == 0
                || strcmp( argv[c], "-help" ) == 0
                || strcmp( argv[c], "--help" ) == 0
                || strcmp( argv[c], "-version" ) == 0
                || strcmp( argv[c], "-Version" ) == 0
                )
            {
            print_usage( argv );
            exit (0);
            }
        else if (argv[c][0] == '-')
            {
            // unrecognized switch
            print_usage( argv );
            exit (1);
            }
        
        }
}

/******************************************************************************/

int main (int argc, char *argv[])
{
    parse_arguments( argc, argv );

    const char *input_filename = argv[argc-1];
    
    char *input_buffer = new char[ gStreamBufferSize ];
    char *output_buffer = new char[ gStreamBufferSize ];
    
    // open intput file
    ifstream input_file;
    input_file.rdbuf()->pubsetbuf( input_buffer, gStreamBufferSize );
    input_file.open(input_filename, ios::in | ios::binary );
    
    // get size of input
    input_file.seekg( 0, ios::end );
    pos_type input_size = input_file.tellg();
    
    if (input_size <= 0 || input_file.fail())
        {
        cout << "ERROR: Could not open input file " << input_filename << "\n";
        return -1;
        }
    
    if (gFuzzEndPoint == 0 || gFuzzEndPoint > input_size)
        gFuzzEndPoint = input_size;
    
    if (gOutputFilename == NULL)
        gOutputFilename = defaultOutputName;
    
    if (gInputPattern == NULL || gInputLength <= 0)
        {
        gInputPattern = kDefaultInputPattern;
        gInputLength = kDefaultInputLength;
        }
    
    if (gOutputPattern == NULL || gOutputLength <= 0)
        {
        gOutputPattern = kDefaultOutputPattern;
        gOutputLength = kDefaultOutputLength;
        }
    
    // generate output file
        {
        fuzz_one_file( gOutputFilename, input_file, input_size, output_buffer );
        if (input_file.fail())
            {
            cout << "ERROR: input file problem\n";
            return -1;
            }
        }

    // we're done, close the input
    input_file.close();
    
    // cleanup our buffers
    delete[] input_buffer;
    delete[] output_buffer;
    
    return 0;
}

/******************************************************************************/
/******************************************************************************/
